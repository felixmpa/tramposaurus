/*var map = L.map('map').setView([25.2473, 55.2939], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


var LeafIcon = L.Icon.extend({
	options: {
		shadowUrl: '../themes/laratify-octobercms-octaskin/assets/img/logo.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76],
	}
});


map.zoomControl.remove();

var greenIcon = new LeafIcon({iconUrl: '../themes/laratify-octobercms-octaskin/assets/img/logo.png'});

L.marker([25.2473, 55.2939], {icon: greenIcon}).bindPopup("I am a green leaf.").addTo(map);
*/


var cities = L.layerGroup();

	L.marker([25.2473, 55.2939]).bindPopup('This is Littleton, CO.').addTo(cities);

	var mbAttr = 'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

	var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
		streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

	var map = L.map('map', {
		center: [25.2473, 55.2939],
		zoom: 15,
		layers: [grayscale, cities]
	});

	var baseLayers = {
		"Grayscale": grayscale,
		"Streets": streets
	};

	var overlays = {
		"Cities": cities
	};

	L.control.layers(baseLayers, overlays).addTo(map);

	map.zoomControl.remove();
