<?php namespace Blackfusion\Features\Components;

use Cms\Classes\ComponentBase;

use Blackfusion\Features\Models\Features;
use Blackfusion\Features\Models\Places;


class Featureschildren extends ComponentBase
{

    public $features;

    public function componentDetails(){
        return [
            'name' => 'Features children',
            'description' => 'List of features days'
        ];
    }
    public function defineProperties(){
        return [
            'slug' => [
                'title'       => 'Slug ',
                'description' => 'Sort Slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }
    public function getSortOrderOptions(){
        return [
            'orden_id asc' => 'ID (ascending)',
            'orden_id desc' => 'ID (descending)'
        ];
    }

    public function onRun(){

        $this->features = $this->loadFeaturesChildren();
    }
    

    protected function loadFeaturesChildren(){

        $slug = $this->property('slug');
        $feature = Features::where('slug', $slug)->lists('place_id','slug');

        if(is_numeric($feature[$slug])){
        
            $features = Features::where('place_id', $feature[$slug])->get();
        
        $query = $features;
        $query = $query->sortBy('orden_id');
        return $query;

        }
    }
}