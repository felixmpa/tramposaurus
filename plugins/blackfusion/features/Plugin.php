<?php namespace Blackfusion\Features;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
   public function registerComponents()
    {
        return [
            'Blackfusion\Features\Components\Featureschildren' => 'features',
        ];
    }


    public function registerFormWidgets()
    {
        return [
            'Blackfusion\Tags\FormWidgets\Tagsbox' => [
                'label' => 'Tagsbox field',
                'code'  =>  'tagsbox'
            ]
        ];
    }


    
    public function registerSettings()
    {
    }



}
