<?php namespace Blackfusion\Features\Models;

use Model;

/**
 * Model
 */
class Features extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_features_posts';

    /**
    * @var array Guarded fields
    */
    protected $guarded = ['*'];

    /**
    * @var array Relations
    */

    public $belongsTo = [
        'featured_place'    => ['Blackfusion\Features\Models\Places', 'key' => 'place_id'],
        'country'    => ['Blackfusion\Maps\Models\Countries', 'key' => 'country_id']

    ];

    public $belongsToMany = [
        'tags' => [
            'Blackfusion\Tags\Models\Tags',
            'table' => 'blackfusion_tags_features_posts',
            'order' => 'name',
            'key'   => 'posts_id'
        ]
    ];

    public $attachOne = [
        'featured_image' => 'System\Models\File'
    ];

      public $attachMany = [
        'featured_images' => 'System\Models\File'
    ];   

}