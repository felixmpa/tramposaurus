<?php namespace Blackfusion\Features\FormWidgets;

use Backend\Classes\FormWidgetBase;

use Config;

use Blackfusion\Features\Models\Features;

class TrekBox extends FormWidgetBase
{
    public function widgetDetails()
    {
        return [
            'name' => 'Trekbox',
            'description' => 'Field for adding treks'
        ];
    }

    public function render(){
        $this->prepareVars();
        return $this->makePartial('widget');
    }

    public function prepareVars(){
        $this->vars['id'] = $this->model->id;

        $this->vars['treks'] = Features::where('parent_id',0)
                                ->where('id', '!=', $this->model->id)
                                ->get()
                                ->lists('title', 'id');

        if(!empty($this->getLoadValue())){
            $this->vars['selectedValues'] = $this->getLoadValue();
        } else {
            $this->vars['selectedValues'] = false;
        }
    }
    
    //public function getSaveValue($treks){
        

        //return ;

        //$newArray = [];
        // foreach($treks as $trekID){
        //     if(!is_numeric($trekID)){
        //         $newTrek = new Features;
        //         $lastFeatures = explode(' ', $trekID);
                
        //         $newTrek->id = $lastFeatures[0];
        //         $newTrek->title = $lastFeatures[1];
        //         $newTrek->save();
        //         $newArray[] = $newTrek->id;
        //     } else {
        //         $newArray[] = $trekID;
        //     }
        // }
        // return $newArray;
    //}

    public function loadAssets()
    {
        $this->addCss('css/select2.css');
        $this->addJs('js/select2.js');
    }
}