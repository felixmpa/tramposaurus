<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionFeaturesPosts5 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->renameColumn('parent_id', 'place_id');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->renameColumn('place_id', 'parent_id');
        });
    }
}
