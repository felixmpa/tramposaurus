<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionFeaturesPlaces2 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_features_places', function($table)
        {
            $table->dropColumn('country');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_features_places', function($table)
        {
            $table->string('country', 255);
        });
    }
}
