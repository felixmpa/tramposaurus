<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionFeaturesPosts extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->integer('parent_id');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->dropColumn('parent_id');
        });
    }
}
