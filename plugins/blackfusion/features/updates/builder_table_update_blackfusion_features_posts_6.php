<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionFeaturesPosts6 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->dropColumn('tags');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->string('tags', 255)->nullable();
        });
    }
}
