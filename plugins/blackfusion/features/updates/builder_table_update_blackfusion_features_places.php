<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionFeaturesPlaces extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_features_places', function($table)
        {
            $table->integer('country_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_features_places', function($table)
        {
            $table->dropColumn('country_id');
        });
    }
}
