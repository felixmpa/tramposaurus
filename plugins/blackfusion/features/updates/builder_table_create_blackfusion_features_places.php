<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionFeaturesPlaces extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_features_places', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('country');
            $table->string('city');
            $table->string('location_number');
            $table->text('description');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_features_places');
    }
}
