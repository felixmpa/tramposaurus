<?php namespace Blackfusion\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionFeaturesPosts2 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->integer('orden_id')->nullable()->change();
            $table->integer('parent_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_features_posts', function($table)
        {
            $table->integer('orden_id')->nullable(false)->change();
            $table->integer('parent_id')->nullable(false)->change();
        });
    }
}
