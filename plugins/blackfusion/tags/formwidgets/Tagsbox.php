<?php namespace Blackfusion\Tags\FormWidgets;

use Backend\Classes\FormWidgetBase;

use Config;

use Blackfusion\Tags\Models\Tags;

class TagsBox extends FormWidgetBase
{
    public function widgetDetails()
    {
        return [
            'name' => 'Tagsbox',
            'description' => 'Field for adding tags'
        ];
    }

    public function render(){
        $this->prepareVars();
        //dump($this->vars['selectedValues']);
        return $this->makePartial('widget');
    }

    public function prepareVars(){
        $this->vars['id'] = $this->model->id;
        $this->vars['tags'] = Tags::all()->lists('name','id');
        $this->vars['name'] = $this->formField->getName().'[]';
        
        if(!empty($this->getLoadValue())){
            $this->vars['selectedValues'] = $this->getLoadValue();
        } else {
            $this->vars['selectedValues'] = [];
        }
    }
    
    public function getSaveValue($tags){
        $newArray = [];

        foreach ($tags as $tagID) {
            if(!is_numeric($tagID)){

                $newTag = new Tags;
                $nameLastTag = explode(' ', $tagID);

                $newTag->name = $nameLastTag[0];
                $newTag->save();
                $newArray[] = $newTag->id;

            }else{
                $newArray[] = $tagID;
            }
        }

        return $newArray;
    }

    public function loadAssets()
    {
        $this->addCss('css/select2.css');
        $this->addJs('js/select2.js');
    }
}