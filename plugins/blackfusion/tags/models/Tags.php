<?php namespace Blackfusion\Tags\Models;

use Model;

/**
 * Tag Model
 */
class Tags extends Model
{

    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_tags';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name'];

    /**
     * @var array slugs fields
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'destinations' => [
            'Blackfusion\Destinations\Models\Destinations',
            'table' => 'blackfusion_tags_destinations_posts',
            'order' => 'name',
            'key'   => 'posts_id'
        ],
        'features' => [
            'Blackfusion\Features\Models\Features',
            'table' => 'blackfusion_tags_features_posts',
            'order' => 'name',
            'key'   => 'posts_id'
        ],
        'treks' => [
            'Blackfusion\Treks\Models\Treks',
            'table' => 'blackfusion_tags_treks_posts',
            'order' => 'name',
            'key'   => 'posts_id'
        ]
    ];

    /**
     * Lists tags for the page
     *
     * @param array $sortOrder 
     * @return self
     */
    public function scopeListTags($query, $sortOrder)
    {
        $sortOrder = explode(' ', $sortOrder);
        $sortedBy = $sortOrder[0];
        $direction = $sortOrder[1];

        return $query->orderBy($sortedBy, $direction);
    }
    
}
