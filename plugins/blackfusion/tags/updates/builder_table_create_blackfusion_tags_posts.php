<?php namespace Blackfusion\Tags\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionTagsPosts extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_tags_destinations_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('tags_id')->unsigned();
            $table->integer('posts_id')->unsigned();
            $table->primary(['tags_id','posts_id']);

        });

        Schema::create('blackfusion_tags_treks_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('tags_id')->unsigned();
            $table->integer('posts_id')->unsigned();
            $table->primary(['tags_id','posts_id']);

        });

        Schema::create('blackfusion_tags_features_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('tags_id')->unsigned();
            $table->integer('posts_id')->unsigned();
            $table->primary(['tags_id','posts_id']);

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_tags_destinations_posts');
        Schema::dropIfExists('blackfusion_tags_treks_posts');
        Schema::dropIfExists('blackfusion_tags_features_posts');

    }
}

        