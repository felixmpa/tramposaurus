<?php namespace Blackfusion\Search;

use System\Classes\PluginBase;

use Blackfusion\Destinations\Models\Destinations;
use Blackfusion\Features\Models\Features;
use Blackfusion\Treks\Models\Treks;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        \Event::listen('offline.sitesearch.query', function ($query) {

            //Search your plugin's contents
            $destinations = Destinations::where('title', 'like', "%${query}%")
            ->orWhere('content', 'like', "%${query}%")
            ->get();

            $features = Features::where('title', 'like', "%${query}%")
            ->orWhere('content', 'like', "%${query}%")
            ->get();

            $treks = Treks::where('title', 'like', "%${query}%")
            ->orWhere('content', 'like', "%${query}%")
            ->get();

            $items1 = $destinations->merge($features);
            $items = $items1->merge($treks);
            

            //Now build a results array
            $results = $items->map(function ($item) use ($query) {
                
                $path = "";
                switch ($item->table) {
                    case 'blackfusion_destinations_posts':
                        $path ="/destinations/";
                        break;
                    case 'blackfusion_features_posts':
                        $path = "/features/";
                        break;
                    case 'blackfusion_treks_posts':
                        $path = "/treks/";
                        break;
                    default:
                        $path = "/blog/";
                        break;
                }

                //If the query is found in the title, set a relevance of 2
                $relevance = mb_stripos($item->title, $query) !== false ? 2 : 1;
            
                return [
                    'title'     => $item->title,
                    'text'      => $item->content,
                    'url'       => $path . $item->slug,
                    'thumb'     => $item->featured_image, // Instance of System\Models\File
                    'relevance' => $relevance
                ];
                
           });

            return [
                //'provider' => 'Destinations', // The badge to display for this result
                'results'  => $results,
            ];

        });
    }
}
