<?php namespace Blackfusion\Maps\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Countries extends Controller
{
    public $implement = ['Backend\Behaviors\ListController'];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Blackfusion.Maps', 'main-menu-maps', 'side-menu-item-countries');
    }
}