<?php namespace Blackfusion\Maps\Models;

use Model;

/**
 * Model
 */
class Continents extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_maps_continents';


    protected $fillable = ['code','name'];

    public $hasMany = [

         'countries' => [
            'Blackfusion\Maps\Models\Countries',
            'table' => 'blackfusion_maps_countries',
            'order' => 'name',
            'key'   => 'continent',
            'otherKey' => 'code'
        ],


    ];
    
}