<?php namespace Blackfusion\Maps\Models;

use Model;

/**
 * Model
 */
class Countries extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_maps_countries';

    /* Relations */


    public $belongsTo = [
        'continents'    => ['Blackfusion\Maps\Models\Continents', 'key' => 'code', 'other_key' => 'continent']
    ];

    public $hasMany = [
        'destinations' => ['Blackfusion\Destinations\Models\Destinations', 'key' => 'country_id'],
        'features'     => ['Blackfusion\Features\Models\Features', 'key' => 'country_id'],
        'treks'        => ['Blackfusion\Treks\Models\Treks', 'key' => 'country_id'],
    ];

}


