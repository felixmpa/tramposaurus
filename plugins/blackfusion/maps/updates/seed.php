<?php namespace Blackfusion\Maps\Updates;

use October\Rain\Database\Updates\Seeder;

class SeedAllTables extends Seeder
{

	/*
	*	Run the database seeds
	*
	*	@return void
	*/
	public function run()
	{
		$this->call(ContinentsTableSeeder::class);
		$this->call(CountriesTableSeeder::class);
		$this->call(CountriesISOTableSeeder::class);
	}
}