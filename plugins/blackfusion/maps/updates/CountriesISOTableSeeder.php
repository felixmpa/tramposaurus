<?php namespace Blackfusion\Maps\Updates;

use Blackfusion\Maps\Models\Countries;

use October\Rain\Database\Updates\Seeder;

use Illuminate\Support\Facades\File;

class CountriesISOTableSeeder extends Seeder
{
	/*
	*	Run the database seeds
	*
	*	@return void
	*/
	public function run()
	{
		$json = File::get("database/data/countriesISO.json");
		$data = json_decode($json);
		foreach ($data->countries as $key => $value) {
			$country = Countries::where('code', $key)
								->orWhere('name', $value->name)
								->update([
										  'latitude' => $value->latitude,
										  'longitude' => $value->longitude
										  ]);
		}
	}
}