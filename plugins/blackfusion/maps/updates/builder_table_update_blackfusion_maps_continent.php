<?php namespace Blackfusion\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionMapsContinent extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_maps_continent', function($table)
        {
            $table->string('code');
            $table->string('latitude');
            $table->string('longitude');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_maps_continent', function($table)
        {
            $table->dropColumn('code');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->increments('id')->unsigned()->change();
        });
    }
}
