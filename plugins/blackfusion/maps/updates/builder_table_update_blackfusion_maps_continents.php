<?php namespace Blackfusion\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionMapsContinents extends Migration
{
    public function up()
    {
        Schema::rename('blackfusion_maps_continent', 'blackfusion_maps_continents');
        Schema::table('blackfusion_maps_continents', function($table)
        {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
    
    public function down()
    {
        Schema::rename('blackfusion_maps_continents', 'blackfusion_maps_continent');
        Schema::table('blackfusion_maps_continent', function($table)
        {
            $table->string('latitude', 255);
            $table->string('longitude', 255);
        });
    }
}
