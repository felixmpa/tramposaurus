<?php namespace Blackfusion\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionMapsContinent extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_maps_continent', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_maps_continent');
    }
}
