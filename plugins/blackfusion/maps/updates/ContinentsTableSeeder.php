<?php namespace Blackfusion\Maps\Updates;

use Blackfusion\Maps\Models\Continents;

use October\Rain\Database\Updates\Seeder;

use Illuminate\Support\Facades\File;

class ContinentsTableSeeder extends Seeder
{
	/*
	*	Run the database seeds
	*
	*	@return void
	*/
	public function run()
	{
		Continents::truncate();
		$json = File::get("database/data/continents.json");
		$data = json_decode($json);
		foreach ($data->continents as $key => $value) {
			Continents::create(array('code'=> $key, 'name' => $value));	
		}
	}
}