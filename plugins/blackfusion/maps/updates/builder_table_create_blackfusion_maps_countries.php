<?php namespace Blackfusion\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionMapsCountries extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_maps_countries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('continent');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_maps_countries');
    }
}
