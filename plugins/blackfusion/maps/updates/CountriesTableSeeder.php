<?php namespace Blackfusion\Maps\Updates;

use Blackfusion\Maps\Models\Countries;

use October\Rain\Database\Updates\Seeder;

use Illuminate\Support\Facades\File;

class CountriesTableSeeder extends Seeder
{
	/*
	*	Run the database seeds
	*
	*	@return void
	*/
	public function run()
	{
		Countries::truncate();
		$json = File::get("database/data/countries.json");
		$data = json_decode($json);
		foreach ($data->countries as $key => $value) {
			Countries::create(array('code'=> $key, 'name' => $value->name, 'continent' => $value->continent));	
		}
	}
}