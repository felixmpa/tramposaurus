<?php

use Blackfusion\Maps\Models\Countries;
use Blackfusion\Tags\Models\Tags;


Route::get('/maps/json', function(){
	
	$json = File::get("database/data/test.geojson");
	$data = json_decode($json);
	$response = Response::make($json, 200);
	$response->header('Content-Type', 'application/json');
	return $response;
});

Route::group(['prefix' => 'api', 'namespace' => 'API' ], function () {
	
	Route::group(['prefix' => 'v1'], function () {
       
		//Get All Countries by Code
		Route::get('/maps/country/{string}',function($string){
		
			$country = Countries::where('code', $string)
								->orWhere('name', $string)
								->with(['destinations','features','treks'])
								->firstOrFail();


			$geo = array("geometry"  => [ "type" => "Point", "coordinates" => []],
						 "type" 	 => "Feature",
						 "properties"=> []
						);

			$tag = array("id" => 0,"name" => "", "total" => 0);

			$collections = [];
			$tags = [];

			if($country->destinations->count()){				
				$destinations = $country->destinations->map(function($item,$key){
					$properties = ['type' => 'Destinations',
								   'title' => $item->title,
								   'slug' => 'destinations/'.$item->slug];
					$latLong = array_reverse(preg_split("/[\s,]+/", $item->location_number));
					$arr['latLong'] = $latLong;
					$arr['properties'] = $properties;
					$arr["featured_image"] = $item->featured_image;
					$arr['tags'] = $item->tags;
					return $arr;
				});				
				
				foreach ($destinations as $key => $value) {
					$obj = json_decode(json_encode($geo), FALSE);
					$obj->geometry->coordinates = $value["latLong"];
					$obj->properties = $value["properties"];
					array_push($collections, $obj);

					foreach($value["tags"] as $key => $value){
						
						if(isset($tags["name"][$value->name])){

						}else{
							$objT = json_decode(json_encode($tag), FALSE);
							$objT->id = $value->id;
							$objT->name = $value->name;
							array_push($tags, $objT);



						}


						
					}

					
				}
			}

			//$unique = array_unique($tags);

			//dd($tags);

			if($country->features->count()){				
				$features = $country->features->map(function($item,$key){
					$properties = ['type' => 'Features',
								   'title' => $item->title,
								   'slug' => 'features/'.$item->slug
								   ];
					$latLong = array_reverse(preg_split("/[\s,]+/", $item->location_number));
					$arr['latLong'] = $latLong;
					$arr['properties'] = $properties;
					$arr["featured_image"] = $item->featured_image;
					$arr['tags'] = $item->tags;
					return $arr;
				});				
				
				foreach ($features as $key => $value) {
					$obj = json_decode(json_encode($geo), FALSE);
					$obj->geometry->coordinates = $value["latLong"];
					$obj->properties = $value["properties"];
					array_push($collections, $obj);
				}
			}

			if($country->treks->count()){				
				$treks = $country->treks->map(function($item,$key){
					$properties = ['type' => 'Treks',
								   'title' => $item->title,
								   'slug' => 'treks/'.$item->slug];
					$latLong = array_reverse(preg_split("/[\s,]+/", $item->location_number));
					$arr['latLong'] = $latLong;
					$arr['properties'] = $properties;
					$arr["featured_image"] = $item->featured_image;
					$arr['tags'] = $item->tags;
					return $arr;
				});				
				
				foreach ($treks as $key => $value) {
					$obj = json_decode(json_encode($geo), FALSE);
					$obj->geometry->coordinates = $value["latLong"];
					$obj->properties = $value["properties"];
					array_push($collections, $obj);
				}
			}

			
			$json = json_encode([
					"type" => "FeatureCollection",
					"features" => $collections,
					"country"  => $country
			]);

		    $response = Response::make($json, 200);
			$response->header('Content-Type', 'application/json');
			return $response;
		});

    });

});



