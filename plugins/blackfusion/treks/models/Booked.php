<?php namespace Blackfusion\Treks\Models;

use Model;

/**
 * Model
 */
class Booked extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_treks_booked';

    protected $fillable = array('starting_date', 'starting_day', 'attendants');

}