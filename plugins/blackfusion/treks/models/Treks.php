<?php namespace Blackfusion\Treks\Models;

use Model;

/**
 * Model
 */
class Treks extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\SimpleTree;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_treks_posts';

    /**
    * @var array Guarded fields
    */
    protected $guarded = ['*'];

    /**
    * @var array Relations
    */

    public $belongsTo = [
        'parent'    => ['Blackfusion\Treks\Models\Treks', 'key' => 'parent_id'],
        'country'    => ['Blackfusion\Maps\Models\Countries', 'key' => 'country_id']
    ];

    public $belongsToMany = [
        'tags' => [
            'Blackfusion\Tags\Models\Tags',
            'table' => 'blackfusion_tags_treks_posts',
            'order' => 'name',
            'key'   => 'posts_id'
        ]
    ];

    public $hasMany = [
        'children'    => ['Blackfusion\Treks\Models\Treks', 'key' => 'parent_id'],
    ];

    public $attachOne = [
        'featured_image' => 'System\Models\File'
    ];

      public $attachMany = [
        'featured_images' => 'System\Models\File'
    ];

      /**
     * Limit results to only records that are eligible to be parents of the provided model.
     * Ineligible parents include: The provided model itself, models with the provided model 
     * as it's own parent already, and a model that is already the current model's parent
     *
     * @param Query $query
     * @param Model $model The model to check for eligible parents against
     * @return Query
     */
    public function scopeEligibleParents($query, $model)
    {
        $query
            ->where('id', '!=', $model->id)
            ->where(function($query) use ($model) {
                $query
                    ->where('parent_id', '!=', $model->id)
                    ->orWhereNull('parent_id')
                ;
            })
        ;
        if ($model->parent_id) {
            $query->where('id', '!=', $model->parent_id);
        }
        return $query;
    }



    /**
     * After booked event
     * @return void
     */
    public function sendBooked()
    {
        /*$this->last_login = $this->last_seen = $this->freshTimestamp();

        if ($this->trashed()) {
            $this->restore();

            Mail::sendTo($this, 'rainlab.user::mail.reactivate', [
                'name' => $this->name
            ]);

            Event::fire('rainlab.user.reactivate', [$this]);
        }
        else {
            parent::afterLogin();
        }

        Event::fire('rainlab.user.login', [$this]);*/
    }


}