<?php namespace Blackfusion\Treks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionTreksBooked extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_treks_booked', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('starting_date');
            $table->string('starting_day');
            $table->string('attendants');
            $table->integer('user_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_treks_booked');
    }
}
