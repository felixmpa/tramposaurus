<?php namespace Blackfusion\Treks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionTreksPosts10 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->integer('country_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->integer('country_id')->nullable(false)->change();
        });
    }
}
