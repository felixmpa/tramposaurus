<?php namespace Blackfusion\Treks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionTreksPosts7 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->integer('parent_id')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->integer('parent_id')->default(0)->change();
        });
    }
}
