<?php namespace Blackfusion\Treks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionTreksPosts extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->integer('trek_parent_id');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->dropColumn('trek_parent_id');
        });
    }
}
