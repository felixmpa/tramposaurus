<?php namespace Blackfusion\Treks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionTreksPosts5 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->integer('parent_id')->nullable()->default(0);
            $table->integer('nest_right')->nullable();
            $table->integer('nest_depth')->nullable();
            $table->integer('nest_left')->nullable()->change();
            $table->dropColumn('trek_parent_id');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_treks_posts', function($table)
        {
            $table->dropColumn('parent_id');
            $table->dropColumn('nest_right');
            $table->dropColumn('nest_depth');
            $table->integer('nest_left')->nullable(false)->change();
            $table->integer('trek_parent_id')->default(0);
        });
    }
}
