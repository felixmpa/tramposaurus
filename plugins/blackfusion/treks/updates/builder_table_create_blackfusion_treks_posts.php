<?php namespace Blackfusion\Treks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionTreksPosts extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_treks_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('content');
            $table->string('location_number')->nullable();
            $table->string('location_address')->nullable();
            $table->string('website')->nullable();
            $table->string('directions')->nullable();
            $table->string('tags')->nullable();
            $table->text('video_url')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_treks_posts');
    }
}
