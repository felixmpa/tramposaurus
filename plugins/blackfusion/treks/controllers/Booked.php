<?php namespace Blackfusion\Treks\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Booked extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Blackfusion.Treks', 'main-menu-treks', 'side-menu-booked');
    }
}