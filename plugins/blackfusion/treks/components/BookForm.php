<?php namespace Blackfusion\Treks\Components;

use Cms\Classes\ComponentBase;
use Input;
use Mail;
use Validator;
use Redirect;
use ValidationException;
use October\Rain\Auth\Manager;
use Auth;

class BookForm extends ComponentBase
{
    public function componentDetails(){
        return [
            'name' => 'Book Trip Form',
            'description' => 'Book trip form'
        ];
    }
    public function onSend(){

        $data = post();
        $rules = [
            'starting-date' => 'required',
            'starting-day'  => 'required',
            'attendants'    => 'required'
        ];
        
        $validator = Validator::make($data, $rules);

        if($validator->fails()){
            throw new ValidationException($validator);
        } else {
        
            $user = Auth::getUser();

            $vars = ['name' => $user->name,
                     'email' => $user->email, 
                     'attendants' => Input::get('attendants'),
                     'starting_date' => Input::get('starting-date'),
                     'starting_day' => Input::get('starting-day')
                    ];

            Mail::sendTo([$user->email => $user->name], 'blackfusion.treks::mail.message', $vars);

        }
    }
}