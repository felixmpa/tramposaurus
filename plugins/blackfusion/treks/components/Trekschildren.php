<?php namespace Blackfusion\Treks\Components;

use Cms\Classes\ComponentBase;

use Blackfusion\Treks\Models\Treks;

class Trekschildren extends ComponentBase
{

    public $treks;

    public function componentDetails(){
        return [
            'name' => 'Treks children',
            'description' => 'List of treks days'
        ];
    }
    public function defineProperties(){
        return [
            'slug' => [
                'title'       => 'Slug ',
                'description' => 'Sort Slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }
    public function getSortOrderOptions(){
        return [
            'orden_id asc' => 'ID (ascending)',
            'orden_id desc' => 'ID (descending)'
        ];
    }

    public function onRun(){

        $this->treks = $this->loadTreksChildren();
    }
    

    protected function loadTreksChildren(){

        $slug = $this->property('slug');

        $treks = Treks::where('slug', $slug)->get();

        foreach ($treks as $trek) {
            if(!is_numeric($trek->parent_id)){
                $treksParent = $treks;
                $treksChildren = Treks::where('parent_id', $trek->id)->get();                
            }else{
                $treksParent = Treks::where('id', $trek->parent_id)->get();
                $treksChildren = Treks::where('parent_id', $treksParent[0]->id)->get();                
            }
        }

        $query = $treksParent->merge($treksChildren);
        
        $query = $query->sortBy('orden_id');
    
        return $query;
    }
}