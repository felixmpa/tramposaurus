subject = "Your Treks has been booked"
==
<p>Hello {{ name }},</p>

<p>Your Treks with Tramposaurus has been booked.</p>

<ul>
    <li>Name: {{ name }}</li>
    <li>Email: {{ email }}</li>
    <li>Starting date: {{ starting_date }}</li>
    <li>Starting day: {{ starting_day }}</li>
    <li>Attendants: {{ attendants }}</li>
</ul>