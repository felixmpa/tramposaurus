<?php namespace Blackfusion\Treks;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Blackfusion\Treks\Components\Trekschildren' => 'treks',
            'Blackfusion\Treks\Components\BookForm' => 'bookform'

        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Blackfusion\Tags\FormWidgets\Tagsbox' => [
                'label' => 'Tagsbox field',
                'code'  =>  'tagsbox'
            ]
        ];
    }
    
    public function registerSettings()
    {
    }
}
