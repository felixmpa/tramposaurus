<?php namespace Blackfusion\Destinations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionDestinationsPosts3 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_destinations_posts', function($table)
        {
            $table->string('slug', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_destinations_posts', function($table)
        {
            $table->string('slug', 255)->nullable(false)->change();
        });
    }
}
