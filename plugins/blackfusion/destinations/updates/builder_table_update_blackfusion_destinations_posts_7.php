<?php namespace Blackfusion\Destinations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionDestinationsPosts7 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_destinations_posts', function($table)
        {
            $table->integer('country_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_destinations_posts', function($table)
        {
            $table->dropColumn('country_id');
        });
    }
}
