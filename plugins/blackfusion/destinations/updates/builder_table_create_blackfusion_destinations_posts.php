<?php namespace Blackfusion\Destinations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionDestinationsPosts extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_destinations_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('content');
            $table->double('location_number', 10, 0);
            $table->string('location_address');
            $table->string('website')->nullable();
            $table->string('directions')->nullable();
            $table->string('tags')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_destinations_posts');
    }
}
