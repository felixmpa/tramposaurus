<?php namespace Blackfusion\Destinations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBlackfusionDestinationsPostsGallery extends Migration
{
    public function up()
    {
        Schema::create('blackfusion_destinations_posts_gallery', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->binary('is_featured_image')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('blackfusion_destinations_posts_gallery');
    }
}
