<?php namespace Blackfusion\Destinations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBlackfusionDestinationsPostsGallery extends Migration
{
    public function up()
    {
        Schema::dropIfExists('blackfusion_destinations_posts_gallery');
    }
    
    public function down()
    {
        Schema::create('blackfusion_destinations_posts_gallery', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 255);
            $table->binary('is_featured_image')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
}
