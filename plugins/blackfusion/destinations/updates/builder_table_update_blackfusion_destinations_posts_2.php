<?php namespace Blackfusion\Destinations\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBlackfusionDestinationsPosts2 extends Migration
{
    public function up()
    {
        Schema::table('blackfusion_destinations_posts', function($table)
        {
            $table->text('video_url');
        });
    }
    
    public function down()
    {
        Schema::table('blackfusion_destinations_posts', function($table)
        {
            $table->dropColumn('video_url');
        });
    }
}
