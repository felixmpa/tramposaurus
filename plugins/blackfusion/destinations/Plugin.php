<?php namespace Blackfusion\Destinations;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerFormWidgets()
    {
    	return [
    		'Blackfusion\Tags\FormWidgets\Tagsbox' => [
    			'label' => 'Tagsbox field',
    			'code'	=>	'tagsbox'
    		]
    	];
    }

    public function registerSettings()
    {
    }

    

}
