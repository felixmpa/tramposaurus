<?php namespace Blackfusion\Destinations\Models;

use Model;

/**
 * Model
 */
class Destinations extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'blackfusion_destinations_posts';


    /* Relations */

    public $belongsTo = [
        'country'    => ['Blackfusion\Maps\Models\Countries', 'key' => 'country_id']
    ];

    public $belongsToMany = [
        'tags' => [
            'Blackfusion\Tags\Models\Tags',
            'table' => 'blackfusion_tags_destinations_posts',
            'order' => 'name',
            'key'   => 'posts_id'
        ]
    ];

    public $attachOne = [
        'featured_image' => 'System\Models\File'
    ];

      public $attachMany = [
        'featured_images' => 'System\Models\File'
    ];
    
}